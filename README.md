# MarsValley

## About this project

First of all this is an community project. :)  
Every person can contribute, read, write and use that code entirely as they want.  
All what do is to follow the EUPL License Agreement (Like AGPL).  
The idea behind this project is it, to help people out, learn together and create an entire game based in Rust.

## Features

A detailed list of all features: [/features](/features/README.md)

## Researches

A detailed list of all researches: [/researches](/researches/README.md)

## Pitch me daddy! Tell me about this game?

You know Stardew Valley? Planting, harvesting, earning money?  
MarsValley is exactly that! You are the first person on the mars, plant some fruits to safe your life.  
Mining some ores to build bigger and better spheres and maybe... Someday you're building enough spheres to settle an entire city.  
What are the key features?

- Auto generated Mars
- 3D
- Planting vegetables
- Mining ores
- Generating oxygen to travel more (better ores are more outside)
- Upgrade your sphere
- Automate your plantation
- Building new spheres for foreigners
- Robots! We all love robots!
- Survival mode, you can die if the oxygen is getting low or you starve
- Multiplayer support
- Some features are missing? Start a pitch and talk about your idea!

### Setting

After years of overpopulation, wars between nations and companies and general resource scarcity the player has been sent to Mars alone, to try colonize for future arrivals.
Contact with earth has broken up, due to an unexpected solar flare hitting the ship on its way to Mars.
Prior to the players arrival, in a last effort to save earth, there were a few boring ships sent to Mars to look for ice.
Said ships bored down as deep as the martian underground allowed,

### Pregame

The player wakes up from cryosleep a short time before the landing, where he can choose different boring locations.
Said locations will differ in difficulty, amount of resources/ice and dangers (meteorites, temperature, regolith/dust storms).
Ice has been found at every location, but in different depths. The ice depths in the "mine"/boring hole may correlate with or set the difficulties of each boring hole.
Each boring hole as been given a name using NATO terms. So f.e. the first boring hole at Arcadia Planitia is called "Alpha Boring Site".
After the boring ship landed on martian rock, it left a "cap" on top of it, providing a visual and radio beacon and landing platform for colonization ships.

### Earlygame

WIP

### Midgame

WIP

### Lategame

WIP

### Main resources

- stone
- metal
- organic matter
- fibers
- Oxygen
- Carbon-dioxide

### Biomes

`Small craters`: Field of multiple craters from small asteroids  
`Big Crater`: A big crater from a large asteroid, with metallic fragments and a core in the middle  
`Black dunes`: Dark sand dunes, dark due to high basalt concentration  
`Gullies`: A network of narrow channels and large slopes or dry riverbeds.  
`Grabens`: Volcanic channel, stretching kilometers away from the volcano

Sources: https://en.wikipedia.org/wiki/Noachis_quadrangle

### Research

#### Ice

`Found or suspected at least at two areas`:
Arcadia Planitia

Sources: https://en.wikipedia.org/wiki/Arcadia_Planitia

Deuteronilus Mensae

Sources: https://en.wikipedia.org/wiki/Deuteronilus_Mensae

Ice on mars evaporates with contact to mars atmosphere instantly, due to its low pressure.

Sources: https://www.nasa.gov/feature/jpl/nasas-treasure-map-for-water-ice-on-mars

Ice is mostly found in little quantities and pores in mars rock, a few meters below the surface

#### Oxygen

`Oxygen concentration 23.5%`: Safe for short term, but muscle and tissue damage over extended period  
`Oxygen concentration 19.5% - 23.5%`: Safe breathing, normal earth  
`Oxygen concentration 16% - 19.5%`: Mental impairment, hyperventilation  
`Oxygen concentration 10% - 14%`: Total exhaustion, no physical activities possible  
`Oxygen concentration 0% - 6%`: Death

Sources: https://sciencing.com/minimum-oxygen-concentration-human-breathing-15546.html

#### Breathing

Air circulated (average) 7 - 8 L/m  
Air circulated (total/earth day) 11000 L  
Oxygen concentration in air before breathing: 20%  
Oxygen concentration in air after breathing: 15%

Sources: https://www.sharecare.com/health/air-quality/oxygen-person-consume-a-day#:~:text=The%20average%20adult%2C%20when%20resting,is%20about%2020%2Dpercent%20oxygen.

Amount exhaled in one earth day: 1,04326kg (average)  
Amount exhaled in one earth day: 8,34608kg (heavy activity)

Sources: https://www.nrdc.org/onearth/waiting-exhale#:~:text=The%20average%20human%20exhales%20about,CO2%20as%20his%20sedentary%20brethren.)

#### Mars atmosphere

- 95.9% Co2
- 2% Ar
- 1.9% n2
- 0.14% o2
- 0.05% Co

Sources: https://en.wikipedia.org/wiki/Atmosphere_of_Earth

#### Earth atmosphere

- 78% n2
- 21% o2
- 0.9% Ar
- 0.04% other gases

Sources: https://en.wikipedia.org/wiki/Mars

#### Raptor Engine

Uses liquid Oxygen (O2) as oxidizer, liquid methane (CH4) as fuel  
Fuel ratio: 78% O2, 22% CH4  
Flow rate: 525 kg/s (from that 78% O2, 22% CH4)  
Combined fuel name in combustion chamber: methalox

Sources: https://en.wikipedia.org/wiki/SpaceX_Raptor

#### SOLs

SOL for one solar day on mars.  
One SOL: 24hours, 39minutes and 35 seconds long  
One martian year: approx. 668 SOLs / 687 earth days / 1.88 earth days

Sources: https://en.wikipedia.org/wiki/Sol_(day_on_Mars)

#### Terms

- LMST - Local Mean Solar Time
- LST - Local Solar Time
- GST - Generic Sol time (same as LMST)
- yestersol - martian "yesterday"
- tosol - martian "today"
- nextersol - tomorrow's Mars day (or "solorrow")
- soliday - holiday from planning the rover's next sol

Sources: https://an.rsl.wustl.edu/msl/mslbrowser/helpPages/mslTerms.aspx?AspxAutoDetectCookieSupport=1

#### Ores on mars

- Volcanic Rocks - Basalt
- Common - Magnesium, Aluminum, Titanium, Iron, Chromium (latter 3 mostly from "black dunes")
- Traces - lithium, cobalt, nickel, copper, zinc, niobium, molybdenum, lanthanum, europium, tungsten, gold
- "Blueberries" - Surface rocks with a concentrated amount of ore in it
- Volcanic landscapes: nickel, copper, titanium, iron, platinum, palladium, chromium
- Gypsum veins - calcium, sulfur, water
- Ores produced from asteroid impact ON EARTH: iron, uranium, gold, copper, nickel

Sources: https://en.wikipedia.org/wiki/Ore_resources_on_Mars

#### RESOURCES

- https://an.rsl.wustl.edu/msl/mslbrowser/helpPages/mslTerms.aspx?AspxAutoDetectCookieSupport=1
- https://en.wikipedia.org/wiki/NATO_phonetic_alphabet
- https://en.wikipedia.org/wiki/Ore_resources_on_Mars

## Rust?! Dafuq? WhY aRE YoU NOt UsINg UNiTy?!

I know ... Rust in game development? Kinda sus... But tbh, Rust rocks!  
Maybe Rust is a little bit harsh for new people and maaaaaybe it's not the perfect language for game development.  
"But Rust is the fastest language, blablabla": Yeah I know, Rust is one of the fastest and safest language in the world.  
And all that features sounding great, but Rust has some problems ... problems why I said it's not the best language for game development.  
The frameworks..., there are some awesome frameworks out there, but the features and documentation is waaaaaaaaay behind competitors like "Unity", or "Godot".  
"Is it bad for our purpose?": Not really. I mean of course it will be a hard time, but it's also an opportunity for us to learn help out with the documentation.  
As an example the bevy library is one of the best examples how beautiful a framework can be, but the lack of documentation can be quit frustrating.  
So instead of just take the knowledge for our self, we can contribute our knowledge and help out with the documentation. :)
tldr; Yes Rust is not the best language for game engines, but it'll be a great journey for us (hopefully).

## Where Code?

The group [MarsValley](https://gitlab.com/marsvalley) is split up into multiple repositories, to keep the things clean.  
For more informations, look below. :)

[MarsValley: ](https://gitlab.com/marsvalley/marsvalley) Game wiki and documentation  
[Bevy: ](https://gitlab.com/marsvalley/bevy) Fork from [GitHub Bevy Repository](https://github.com/bevyengine/bevy), documentation  
[Game: ](https://gitlab.com/marsvalley/game) The game itself  
[Assets (private): ](https://gitlab.com/marsvalley/assets) The paid assets, license agreement still in progress

### MarsValley (ticket system, documentation, wiki, download compiled game)

In this repository (this, there you are right now, exactly! Right there. :3) you can find the documentation for the game itself.  
Imagine we would have an multiplayer and mod support in the future, wouldn't that be nice?  
And here, exactly here you would find the documentation for the mod support! :)  
In the future there will also be the full game downloadable in this readme.... But yeah... still in progress. :3  
You wanna contribute and search for some open issues? There you have it! :)

#### How to reasearch

If you doing some researches, it would be great if you could create a wiki page for that! :)  
The main benefit of that project is it, to have an entire encyclopedia how game engines work and why we choose some approaches.  
Would be great if every newcomer could just read and search in that repository, to solve their problems. :3

### Bevy (Remote repository fork, just for adding engine documentation)

The bevy community is great, the engine itself is awesome! You know what would be more awesome? Exact! Documentation.  
The bevy documentation is ... special ..., often it's not very clear what every parameter does.  
But with the power of the community, we can contribute into them! :)  
If you know what some parameter or functions are doing, just create a branch and make a merge request.  
When the repository is going to be bigger and better, we are going to merge all the changes into the github bevy repository.  
Keep in mind that you will NOT be listed as maintainer on github.
Maybe we find a better solution for that, but right now it's the fastest workflow to just push the documentation into their limits. :)

### Game (Core and game logic)

Here you have the game code, you can fork that repository, but you don't need to fork it.  
We are just a few people, if you work on a ticket, just create a new branch and make a pull request. :3  
Keep in mind to set the `draft` flag it it's not "production" ready. :)
For more information look at the `How to contribute` area in that repository. :3

### Assets (Paid assets, repository is private)

Right now there are assets which are paid and bounded to one seat.  
Maybe I'll find a solution for that problem. WIP

## Boards

### Development

https://gitlab.com/marsvalley/marsvalley/-/boards/4009347

### Research

https://gitlab.com/marsvalley/marsvalley/-/boards/4009388
