# CONTRIBUTING

## How to contribute

1. Search for issues you find interessting in [MarsValley](https://gitlab.com/marsvalley/marsvalley/-/issues)
2. Gain access by ask them in [Discord](https://discord.gg/KgkwCjTeRE) or GitLab.
3. Create branch which start with `feature/` or `fix/`.
4. Create a merge request and mark them as `draft`.
5. Contribute your code.
6. Don't forget the `license header` for new files.
7. Remove your `draft` if you're done.
8. Wait for feedback.
9. Be an awesome contributor! Hurray!

## Default File Formats

In the following we describe the default file formats we use for assets in the
game. If there are better alternatives please open an issue that explains why
we should use it and how a migration would look like.

### Models

For any models we use in the game we are using `gltf` files. This format is
supported by the Khronos Group and supports 3D model geometry, appearance,
scene graph hierarchy, and animation. For further information and additional
tooling around this format consult the [glTF
repository](https://github.com/KhronosGroup/glTF)

### Audio 

For audio files we use the ogg file format with a 16 bit sampling rate and 44.1
Khz. This quality is sufficient for games and also desirable small in file
size.

### Textures

Any textures that are not related to models are in `tga` file format. This file
format is commonly used in game development and also supports alpha channel and
lossless RLE compression to reduce file size.

## Recommended Tools

For building and running the Game you at least need the stable rust toolchain.

### Models

Any software that can export to `gltf` or a file format that can be trivial
converted to `gltf` can be used to create models for the game. We recommend
Blender as it is open source and commonly used.

## License

### License agreement

You agree that your code will be licensed under the EUPL 1.2 or later license.  
And it's NOT possible to change the license WITHOUT the permission from EVERY contributor! :)  

### License header on every source file

Every file needs a `license header` for the `EUPL 1.2 or later` license.  
```rust
/**
 * Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
 * and the contributors of the MarsValley project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */
```
Hint: Don't worry I am (Daniél Kerkmann) NOT the only owner of that code/project.  
It's forbidden to add a copyright header for a fictional name (which is not a registered trademark or registerd company).  
So I'm just the person who can act legally if people using OUR code WITHOUT applying the EUPL license. :)

### Why the EUPL 1.2 or later?

The EUPL 1.2 is a very strict license, like the GNU Affero General Public License (AGPL).  
It protects OUR code from absuing them.  
If the project would be licensed under the MIT license, a person could just download our code, build a binary and sell that binary WITHOUT noticing that OUR code was used.  
The EUPL is protecting US against it, the source code needs to be linked (like the main menu in a game).  :)
One of the reasons to use the EUPL instead of the AGPL is the legal certainty.  
The license itself is agreed from an european commitee in every common language.  
Find out more on: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
