# Day-Night cycle

The day and night cycle in general is not linear. On the south side it's
shorter than on the north side. The day and night cycle is depending on the
season for example in winter season the day is
[shorter](https://www.laenderdaten.info/Europa/Deutschland/sonnenuntergang.php)
as in summer. The Mars has a longer day as the Earth.

On Mars a day has 24 hours and 40 minutes.  
There is also a hot period that could be called _summer_.
If you compare the Earth with the Mars, the Earth needs twice the time to do a complete rotation.
That means on the Earth a rotation (passing all seasons) take one year.  
On the Mars an entire rotation takes two years.
Read more in [this](https://www.futurezone.de/science/article276026/wie-lange-dauert-ein-tag-auf-dem-mars.html) German article about thaa

The sunset on the mars is
[blue](https://www.pm-wissen.com/weltraum/a/welche-farbe-hat-ein-sonnenuntergang-auf-dem-mars/8670/).
