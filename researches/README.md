# Researches

### List of researches

- [Day-Night cycle](/researches/day-night-cycle.md)

### Contribute new researches

You wanna contribute your researches?  
Just do some research, create a new file in the `researches` folder and add them to the `List of researches` list.
