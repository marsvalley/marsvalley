FROM rust:latest as planner
WORKDIR /cache
COPY ./game .
RUN cargo install cargo-chef
RUN cargo chef prepare --recipe-path recipe.json

FROM rust:latest as cacher
WORKDIR /cache
COPY --from=planner /cache/recipe.json ./recipe.json
COPY --from=planner /usr/local/cargo/bin/cargo-chef /usr/local/cargo/bin/cargo-chef
RUN apt-get update && apt-get install pkg-config libasound2-dev libudev-dev mingw-w64 -y
RUN rustup target add x86_64-apple-darwin
RUN rustup target add x86_64-pc-windows-gnu
RUN rustup target add x86_64-unknown-linux-gnu
RUN cargo chef cook --release --recipe-path ./recipe.json
