# World related features

## List of features

- [Auto generated terrain](#auto-generated-terrain)

### Auto generated terrain

`Description`: The terrain should be auto generated. \
`Why`: To create randomness into that game, so not every world will be the same. \
`How`: Procedural generation with random seed.
