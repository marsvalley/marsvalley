# Player related features

## List of features

- [Health](#health)
- [Stamina](#stamina)
- [Hunger](#hunger)
- [Thirst](#thirst)

### Health

`Description`: Health for the player, so he can die. \
`Why`: Would be cool to have some difficulty, like monsters or starving. \
`How`: Simple health system (min, max, current). Decrease on interactions (monster, starving, etc.), reset on sleeping.

### Stamina

`Description`: Player stamina which will decrease on interactions. \
`Why`: The interactions per day will be decreased, so it's harder to get into the late game. \
`How`: Simple stamina system (min, max, current). Decrease on actions, reset on sleeping.

### Hunger

`Description`: Player are like humans, they need food! \
`Why`: Like a real person, the player should also take care of their hunger. Otherwise planting some vegetables would be useless. \
`How`: Simple hunger system (min, max, current). Decrease per tick, increase by eating.

### Thirst

`Description`: Player are like humans, the need water! \
`Why`: Like a real person, the player should also take care of their thirst. Otherwise generating water would be useless. \
`How`: Simple drink system (min, max, current). Decrease per tick, increase by drinking.
