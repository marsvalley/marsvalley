# Features

### List of features

- [Player related features](/features/player.md)
- [World related features](/features/world.md)

### Contribute new features

You wanna contribute your ideas?  
Just use this template and bring up your ideas! :)

```
`Description`: A small description about the feature. \
`Why`: A reason why the feature should be implemented. \
`How`: A simple instruction how it could look like.
```
